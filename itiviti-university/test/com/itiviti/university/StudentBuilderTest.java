package com.itiviti.university;

import static org.junit.Assert.*;

import java.util.Date;

import org.junit.Test;

public class StudentBuilderTest {

    @Test
    public void buildStudentWithFirstName() {
        assertEquals("John", Student.builder().firstName("John").build().getFirstName());
    }

    @Test
    public void buildStudentWithLastName() {
        assertEquals("Doe", Student.builder().lastName("Doe").build().getLastName());
    }
    
    @Test
    public void buildStudentWithGpa() throws Exception {
        assertEquals(4.51, Student.builder().gpa(4.51).build().getGpa(), 0.01);
    }

    @Test
    public void buildStudentWithEnrollmentDate() throws Exception {
        Date date = new Date();
        assertEquals(date, Student.builder().enrollment(date).build().getEnrollmentDate());
    }
    
    @Test
    public void builderCreatesMultipleUniqueStudents() throws Exception {
        StudentBuilder builder = Student.builder().firstName("John").lastName("Doe");
        Student johnDoe = builder.build();
        Student johnSmith = builder.lastName("Smith").build();
        assertEquals("Doe", johnDoe.getLastName());
        assertEquals("Smith", johnSmith.getLastName());
    }
}
