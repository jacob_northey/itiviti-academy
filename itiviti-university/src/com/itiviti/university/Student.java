package com.itiviti.university;

import java.util.Date;

public class Student {
    static final int UNINITIALIZED = 0;
    private String firstName;
    private String lastName;
    private double gpa;
    private Date enrollmentDate;
    private Date birthDate;
    private int id = UNINITIALIZED;
    
    public Student() {
    }

    private Student(Student prototype) {
        this.firstName = prototype.firstName;
        this.lastName = prototype.lastName;
        this.gpa = prototype.gpa;
        this.enrollmentDate = prototype.enrollmentDate;
        this.birthDate = prototype.birthDate;
    }

    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public double getGpa() {
        return gpa;
    }
    public void setGpa(double gpa) {
        this.gpa = gpa;
    }
    public Date getEnrollmentDate() {
        return enrollmentDate;
    }
    public void setEnrollmentDate(Date enrollmentDate) {
        this.enrollmentDate = enrollmentDate;
    }
    public Date getBirthDate() {
        return birthDate;
    }
    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }
    
    public static StudentBuilder builder() {
        return new StudentBuilder();
    }
    public Student clone() {
        return new Student(this);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    @Override
    public String toString() {
        return firstName + " " + lastName;
    }
}
