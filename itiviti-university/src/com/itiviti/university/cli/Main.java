package com.itiviti.university.cli;

public class Main {

    public static void main(String[] args) {
        new UniversityCommandLineInterface().start();
    }

}
