package com.itiviti.university.cli;

import com.itiviti.university.UniversityContext;

public class ExitCommand extends AbstractCommand {

    private UniversityCommandLineInterface cli;

    public ExitCommand(UniversityCommandLineInterface cli) {
        super("exit", false);
        this.cli = cli;
    }

    @Override
    public CommandOperation parse(String[] options) {
        return new CommandOperation() {
            @Override
            public void run(UniversityContext context) {
                cli.stop();
            }};
    }
}
