package com.itiviti.university.cli;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Stack;

import com.itiviti.university.StudentRepository;
import com.itiviti.university.UniversityContext;

public class UniversityCommandLineInterface {
    private boolean running;
    private Map<String, Command> commands = new HashMap<>();
    private Stack<CommandOperation> commandHistory = new Stack<>();
    private StudentRepository repository;
    private UniversityContext context;
    private Stack<CommandOperation> commandFuture = new Stack<>();
    
    {
        register(new ExitCommand(this));
        this.repository = new StudentRepository();
        this.context = new UniversityContext();
        context.setRepository(repository);
        register(new CreateStudentCommand());
        register(new DeleteStudentCommand());
        register(new ListStudentsCommand());
        register(new UndoCommand(this));
        register(new RedoCommand(this));
    }

    public void start() {
        UniversityContext context = new UniversityContext();
        context.setRepository(repository);
        try(Scanner scanner = new Scanner(System.in)) {
            running = true;
            while (running) {
                System.out.print(">");
                String command = scanner.next();
                String normalizedCommand = normalizeCommandName(command);
                String[] args = parseArgs(scanner.nextLine());
                if (commands.containsKey(normalizedCommand)) {
                    Command cmd = commands.get(normalizedCommand);
                    try {
                        CommandOperation task = cmd.parse(args);
                        task.run(context);
                        if (cmd.isUndoable()) {
                            this.commandHistory.push(task);
                            commandFuture.clear();
                        }
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                } else {
                    System.out.println("The command \"" + command + "\" does not exist.");
                }
            }
        }
    }

    private String[] parseArgs(String nextLine) {
        String[] rawArgs = nextLine.split(" ");
        List<String> args = new ArrayList<>(rawArgs.length);
        for (String s : rawArgs) {
            String trimmed = s.trim();
            if (!"".equals(trimmed)) {
                args.add(trimmed);
            }
        }
        return args.toArray(new String[args.size()]);
    }

    private void register(Command command) {
        commands.put(normalizeCommandName(command.getCommand()), command);
    }

    private String normalizeCommandName(String command) {
        return command.toUpperCase();
    }

    public void stop() {
        running = false;
    }

    public void undo() {
        if (commandHistory.isEmpty()) {
            System.out.println("No more commands to undo.");
            return;
        }
        CommandOperation lastCommand = commandHistory.pop();
        CommandOperation undoTask = lastCommand.getUndoTask();
        if (undoTask != null) {
            undoTask.run(context);
        }
        commandFuture.push(lastCommand);
    }

    public void redo() {
        if (commandFuture.isEmpty()) {
            System.out.println("No commands to redo.");
            return;
        }
        CommandOperation futureCommand = commandFuture.pop();
        futureCommand.run(context);
        commandHistory.push(futureCommand);
    }
}
