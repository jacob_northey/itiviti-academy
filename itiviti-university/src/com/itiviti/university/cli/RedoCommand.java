package com.itiviti.university.cli;

import com.itiviti.university.UniversityContext;

public class RedoCommand extends AbstractCommand {

    private UniversityCommandLineInterface cli;

    public RedoCommand(UniversityCommandLineInterface cli) {
        super("redo", false);
        this.cli = cli;
    }

    @Override
    public CommandOperation parse(String[] args) {
        return new CommandOperation() {
            @Override
            public void run(UniversityContext context) {
                cli.redo();
            }
        };
    }

}
