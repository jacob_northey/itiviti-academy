package com.itiviti.university.cli;

@SuppressWarnings("serial")
public class TaskCreationException extends RuntimeException {

    public TaskCreationException(String message) {
        super(message);
    }

}
