package com.itiviti.university.cli;

import com.itiviti.university.Student;
import com.itiviti.university.UniversityContext;

public class DeleteStudentTask implements CommandOperation {

    private int id;
    private Student deletedStudent;

    public DeleteStudentTask(int id) {
        this.id = id;
    }

    @Override
    public void run(UniversityContext context) {
        this.deletedStudent = context.getRepository().delete(id);
    }

    @Override
    public CommandOperation getUndoTask() {
        return new CreateStudentTask(deletedStudent);
    }

}
