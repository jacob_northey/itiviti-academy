package com.itiviti.university.cli;

import com.itiviti.university.Student;
import com.itiviti.university.UniversityContext;

public class CreateStudentTask implements CommandOperation {
    private Student student;
    
    public CreateStudentTask(Student student) {
        this.student = student;
    }

    public CreateStudentTask(String firstName, String lastName) {
        this.student = Student.builder().firstName(firstName).lastName(lastName).build();
    }

    @Override
    public void run(UniversityContext context) {
        context.getRepository().save(student);
        System.out.println("Created student " + student);
    }

    @Override
    public CommandOperation getUndoTask() {
        return new DeleteStudentTask(student.getId());
    }
}
