package com.itiviti.university.cli;

public class DeleteStudentCommand extends AbstractCommand {
    public DeleteStudentCommand() {
        super("delete-student", true);
    }

    @Override
    public CommandOperation parse(String[] args) {
        try {
            if (args.length == 0) {
                throw new TaskCreationException("Please supply the id of the student to delete.");
            }
            return new DeleteStudentTask(Integer.parseInt(args[0]));
        } catch (NumberFormatException e) {
            throw new TaskCreationException("The supplied parameter \"" + args[0] + "\" is not a number.");
        }
    }
}
