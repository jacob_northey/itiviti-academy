package com.itiviti.university.cli;

public interface Command {

    String getCommand();

    CommandOperation parse(String[] args);

    boolean isUndoable();

}
