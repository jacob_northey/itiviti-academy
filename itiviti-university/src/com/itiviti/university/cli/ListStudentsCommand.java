package com.itiviti.university.cli;

import java.util.Collection;

import com.itiviti.university.Student;
import com.itiviti.university.UniversityContext;

public class ListStudentsCommand extends AbstractCommand {
    public ListStudentsCommand() {
        super("list-students", false);
    }

    @Override
    public CommandOperation parse(String[] args) {
        return new CommandOperation() {
            @Override
            public void run(UniversityContext context) {
                Collection<Student> students = context.getRepository().getStudents();
                if (students.isEmpty()) {
                    System.out.println("No students exist in the repository.");
                }
                for (Student student : students) {
                    System.out.printf("%3d %-25s\n", student.getId(), student.toString());
                }
            }
        };
    }

    @Override
    public String getCommand() {
        return "list-students";
    }

}
