package com.itiviti.university.cli;

public class CreateStudentCommand extends AbstractCommand {
    public CreateStudentCommand() {
        super("create-student", true);
    }

    @Override
    public CommandOperation parse(String[] options) {
        if (options.length < 2) {
            throw new TaskCreationException("Creating a student requires both a first and last name.");
        }
        return new CreateStudentTask(options[0], options[1]);
    }

    @Override
    public String getCommand() {
        return "create-student";
    }

}
