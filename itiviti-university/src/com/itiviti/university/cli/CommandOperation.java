package com.itiviti.university.cli;

import com.itiviti.university.UniversityContext;

public interface CommandOperation {

    void run(UniversityContext context);
    
    default CommandOperation getUndoTask() {
        return null;
    }
}
