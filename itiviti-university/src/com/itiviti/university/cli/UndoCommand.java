package com.itiviti.university.cli;

import com.itiviti.university.UniversityContext;

public class UndoCommand extends AbstractCommand {

    private UniversityCommandLineInterface cli;

    public UndoCommand(UniversityCommandLineInterface cli) {
        super("undo", false);
        this.cli = cli;
    }

    @Override
    public CommandOperation parse(String[] options) {
        return new CommandOperation() {
            @Override
            public void run(UniversityContext context) {
                cli.undo();
            }
        };
    }

    @Override
    public String getCommand() {
        return "undo";
    }

}
