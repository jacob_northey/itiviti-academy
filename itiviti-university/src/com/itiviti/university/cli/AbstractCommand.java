package com.itiviti.university.cli;

public abstract class AbstractCommand implements Command {
    
    private String command;
    private boolean undoable;

    public AbstractCommand(String command, boolean undoable) {
        this.command = command;
        this.undoable = undoable;
    }

    @Override
    public String getCommand() {
        return command;
    }

    @Override
    public boolean isUndoable() {
        return undoable;
    }

}
