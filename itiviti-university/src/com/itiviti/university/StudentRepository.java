package com.itiviti.university;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public class StudentRepository {

    private Map<Integer, Student> students = new LinkedHashMap<>();
    private int nextStudentId = 1;

    public void save(Student student) {
        if (student.getId() == Student.UNINITIALIZED) {
            student.setId(nextStudentId);
            nextStudentId++;
        }
        students.put(student.getId(), student);
    }

    public Student delete(int id) {
        if (!students.containsKey(id)) {
            throw new RuntimeException("The student with id " + id + " does not exist.");
        }
        Student student = students.remove(id);
        System.out.println("Deleted student " + student);
        return student;
    }

    public Collection<Student> getStudents() {
        return students.values();
    }

}
