package com.itiviti.university;

public class UniversityContext {

    private StudentRepository repository;

    public void setRepository(StudentRepository repository) {
        this.repository = repository;
    }
    
    public StudentRepository getRepository() {
        return repository;
    }
}
