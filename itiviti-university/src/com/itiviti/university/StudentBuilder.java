package com.itiviti.university;

import java.util.Date;

public class StudentBuilder {
    Student prototype = new Student();

    public Student build() {
        return prototype.clone();
    }
    
    public StudentBuilder firstName(String first) {
        prototype.setFirstName(first);
        return this;
    }
    
    public StudentBuilder lastName(String last) {
        prototype.setLastName(last);
        return this;
    }
    
    public StudentBuilder enrollment(Date date) {
        prototype.setEnrollmentDate(date);
        return this;
    }
    
    public StudentBuilder birth(Date date) {
        prototype.setBirthDate(date);
        return this;
    }
    
    public StudentBuilder gpa(double gpa) {
        prototype.setGpa(gpa);
        return this;
    }
}
